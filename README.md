#Laboratório de Organização e Arquitetura de Computadores 1#
#Prof. Dr. Edilson Kato#

##Projeto final da disciplina realizado por Lucas Mendes e Thales Borzani##

Este é um repositóro que contém arquivos para compilação e simulação de duas arquiteturas, de Von Neumman e Harvard, implementadas em MIPS.

###O relatório final, ou seja, explicação teórica e dos procedimentos e também simulações, estão na Wiki desse mesmo repositório###